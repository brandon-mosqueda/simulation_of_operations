library(lpSolve)

# Matriz con costes de transportes
cost <- matrix(c(3, 4, 6, 8, 9,
                 2, 2, 4, 5, 5,
                 2, 2, 2, 3, 3,
                 3, 3, 2, 4, 2),
               nrow = 4, ncol = 5, byrow = TRUE)

direction <- "min"
row.signs <- rep("=", 4)
# Número de elementos disponibles de cada producto
row.rhs <- c(30, 80, 10, 60)
col.signs <- rep("=", 5)
# Número de demanda de productos por cada lugar
col.rhs <- c(10, 50, 20, 80, 20)

sol <- lp.transport(cost, direction, row.signs, row.rhs,
                    col.signs, col.rhs)

colnames(sol$solution) <- paste0("Lugar_", 1:5)
rownames(sol$solution) <- paste0("Producto_", 1:4)
sol$solution
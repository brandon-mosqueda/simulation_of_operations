Example 1

Imagine we need to solve the following linear programming task: Maximize
  FORMULA 1 (214)

subject to
  FORMULA 2 (214)

and x 1 , x 2 ≥ 0, but the variables x 1 , x 2 need to be integers, for example because we cannot break our product in smaller parts. If this problem is solved with lpSolve, the solution will be x 1 = 6 37 , x 2 = 4 27 (with y = 5142 67 ). Taking the integer parts, will x 1 = 6, x 2 = 4 (with y = 4800) be the optimal solution in integers? Fortunately, lpSolve, as well as all the other LP solvers mentioned in the previous section, are capable of solving mixed-integer linear programs where some or all of the variables have to be binary (taking on only 0 or 1 as values) or integers. The user has to explicitly declare which of the variables fall into these classes; all others are still assumed to be positive reals.

obj <- c(500, 450)
A <- matrix(c(6, 5,
              10, 20,
              1, 0), ncol=2, byrow=TRUE)
b <- c(60, 150, 8)

# Declare which variables have to be integer (here all of them)
int.vec <- c(1, 2)
soln <- lp("max", obj, A, rep("<=", 3), b, int.vec=int.vec)
soln
# Success: the objective function is 4900

soln$solution
# [1] 8 2

One can see that the solution (8, 2) is better than the nearest integer solution (6, 4), and is not even a direct "neighbor" in the integer grid. That means guessing from the unconstrained, real solution is not an appropriate step.

Example 2

As another example, let us assume that x 1 and x 2 are allowed to be real, but the second variable is semi-continuous, that is either x 2 = 0 or x 2 ≥ 3; it is not allowed to take on values between 0 and 3. This constraint cannot be expressed as a linear constraint. Instead, we introduce a new binary variable b that is assumed to be 0 if x 2 is zero, and 1 if not. So x 2 ≥ 3b will guarantee that x 3 , if not 0, is greater than 3. Of course, if b = 0 then x 2 shall be 0, too. This can be expressed with another constraint x 2 ≤ Mb where M is some large enough constant, e.g., we can assume M = 20. (In the literature on MILP, this is called the "big-M" trick.) With variables x 1 , x 2 , b the set of constraints now look like
  FORMULA 3 (215)

and thus
obj <- c(500, 400, 0)
A <- matrix(c(6, 5, 0,
              10, 20, 0,
              1, 0, 0,
              0, -1, 3,
              0, 1, -20), ncol=3, byrow=TRUE)
b <- c(60, 150, 8, 0, 0)
int.vec <- c(3)
soln <- lp("max", obj, A, rep("<=", 5), b, int.vec = int.vec)
soln
# Success: the objective function is 4950

soln$solution
# [1] 7.5 3.0 1.0

Semi-continuous variables and the "big-M" trick are often-used elements of modeling and solving linear programming tasks.